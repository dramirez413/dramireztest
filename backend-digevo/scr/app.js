import Express from 'express';
import { urlencoded, json } from 'body-parser';

import authRoutes from './routes/auth';
import newsRoutes from './routes/news';

let app = Express();

app.use(function(req, res, next) {
  //console.log(req.headers.origin);
  res.header("Access-Control-Allow-Origin", '*');
  res.header("Access-Control-Allow-Methods", 'GET, POST, PUT, OPTIONS, DELETE, PATCH');
  res.header("Access-Control-Allow-Credentials", "false");
  res.header("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

app.use(urlencoded({extended:true, defaultCharset: 'utf-8'}));
app.use(json());

app.use('/api/auth', authRoutes);
app.use('/api/news', newsRoutes);

export default app;