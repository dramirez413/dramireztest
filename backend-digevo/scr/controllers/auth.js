import empty from 'is-empty';
import jwt from 'jsonwebtoken';
import { connection } from '../models/connection';
import bcrypt from 'bcryptjs';

const test = (req, res, next) => {
  let query = `SELECT * FROM person`
  connection.any(query)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => next(err))
}

const login = (req, res, next) => {
  if (!empty(req.body.username) && !empty(req.body.password)) {
    let query = `SELECT id, name, surname, username, password FROM person WHERE username = $(username)`;
    connection.any(query, { username: req.body.username })
      .then(data => {
        if (!empty(data)) { 
          bcrypt.compare(req.body.password, data[0].password).then((compare) => {
            if (compare === true) {
              let token = jwt.sign({ foo: 'digevo' }, 'thebestjob!');
              let userInfo = {
                id: data[0].id,
                token: token,
                name: data[0].name,
                surname: data[0].surname,
                username: data[0].username,
                login: false
              };
              // UPDATE TOKEN USER LOGIN
              let queryUpdateToken = `UPDATE person SET token = $(token) WHERE id = $(id)  RETURNING id as personId`;
              let dataUpdateToken = {
                id: userInfo.id,
                token: userInfo.token
              };
              //res.status(200).json(userInfo);
              connection.tx(t => {
                return t.batch([
                  t.one(queryUpdateToken, dataUpdateToken),
                ]);
              })
                .then(dataUpdate => {
                  userInfo.login = true;
                  res.status(200).json(userInfo);
                })
                .catch(error => {
                    res.status(500).json({'success': false, 'error': error});
                });
            } else {
              res.status(204).json();
            }
          });
        } else {
          res.status(204).json();
        }
      })
      .catch(err => {
        res.status(500).json({'success': false, 'error': error});
      });
  } else {
    res.status(400).json({data: 'Bad request'});
  }
}

const logout = (req, res, next, userData) => {
  let queryLogout = `UPDATE person SET token = $(token) WHERE id = $(id) RETURNING id as personId`;
  let dataLogout = {
    id: userData.id,
    token: null
  };
  connection.tx(t => {
    return t.batch([
      t.one(queryLogout, dataLogout),
    ]);
  })
    .then(dataLogout => {
      res.status(200).json({ logout: true });
    })
    .catch(error => {
        res.status(500).json({'success': false, 'error': error});
    });
}

const token = (req, res, next, userData) => {
  let query = `SELECT * FROM person WHERE token = $(token)`;
  let queryData = {
    token: userData.token
  }
  connection.any(query, queryData)
    .then(data => {
      res.status(200).json();
    })
    .catch(err => res.status(400).json())
}

const password = (req, res, next) => {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash("1234", salt, (err, hash) => {
      res.status(200).json(hash);
      bcrypt.compare("1234", "$2a$10$W54oCFF8Vr7SQONqiJqBCu7IulBKONalWQvEKy").then((compare) => {
        res.status(200).json(compare);
      });
    });
  });
}
 
export default {
  login,
  logout,
  token,
  password,
  test
};