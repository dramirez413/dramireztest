import empty from 'is-empty';
import jwt from 'jsonwebtoken';
import { connection } from '../models/connection';

const getList = (req, res, next) => {
    let query = `SELECT id, title, description FROM news WHERE state = TRUE ORDER BY id DESC`
    connection.any(query)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => next(err))
};

const insert = (req, res, next, dataUser) => {
    if (!empty(req.body.title) && !empty(req.body.description)) {
        let insertData = {
            personId: dataUser.id,
            title: req.body.title,
            description: req.body.description
        }
        let queryInsert = `INSERT INTO news (personId, title, description) VALUES ($(personId), $(title), $(description)) RETURNING id as newsId`;
        connection.tx(t => {
            return t.batch([
                t.one(queryInsert, insertData),
            ]);
        })
        .then(dataInsert => {
            res.status(200).json(dataInsert[0]);
        })
        .catch(error => {
            res.status(500).json({'success': false, 'error': error});
        });
    } else {
        res.status(400);
    }
};

const update = (req, res, next, dataUser) => {
    if (!empty(req.body.id) && !empty(req.body.title) && !empty(req.body.description)) {
        let updateData = {
            personId: dataUser.id,
            newsId: req.body.id,
            title: req.body.title,
            description: req.body.description
        }
        let queryUpdate = `UPDATE news SET title = $(title), description = $(description), personId = $(personId) WHERE id = $(newsId) RETURNING id as newsId`;
        connection.tx(t => {
            return t.batch([
                t.one(queryUpdate, updateData),
            ]);
        })
        .then(dataUpdate => {
            res.status(200).json(dataUpdate[0]);
        })
        .catch(error => {
            res.status(500).json({'success': false, 'error': error});
        });
    } else {
        res.status(400);
    }
};

const eliminate = (req, res, next) => {
    if (!empty(req.body.id)) {
        let deleteData = {
            id: req.body.id
        }
        let queryDelete = `DELETE FROM news WHERE id = $(id)`;
        connection.tx(t => {
            return t.batch([
                t.none(queryDelete, deleteData),
            ]);
        })
        .then(data => {
            res.status(200).json({ success: true });
        })
        .catch(error => {
            res.status(500).json({ success: false, error });
        });
    } else {
        res.status(400);
    }
};

const state = (req, res, next, dataUser) => {
    if (!empty(req.body.id)) {
        let updateData = {
            newsId: req.body.id
        }
        let queryUpdate = `UPDATE news SET state = NOT state WHERE id = $(newsId) RETURNING id as newsId`;
        connection.tx(t => {
            return t.batch([
                t.one(queryUpdate, updateData),
            ]);
        })
        .then(dataUpdate => {
            res.status(200).json(dataUpdate[0]);
        })
        .catch(error => {
            res.status(500).json({'success': false, 'error': error});
        });
    } else {
        res.status(400);
    }
};
 
export default {
  getList,
  insert,
  update,
  state,
  eliminate
};