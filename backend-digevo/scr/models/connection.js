import Promise from 'bluebird';
import pgp from 'pg-promise';
import empty from 'is-empty';

const postgres = pgp({ promiseLib: Promise });
const user = 'postgres';
const password = 'pokemon';
const ip = 'localhost';
const port = '5432';
const db = 'digevo';
const connection = postgres(`postgres://${user}:${password}@${ip}:${port}/${db}`);

const selectQuery = (res, query, dataQuery = null) => {
  connection.any(query, dataQuery)
    .then(data => {
      if(!empty(data))
        res.status(200).json(data);
      else
        res.status(204).json(); 
    })
    .catch(err => next(err));
};

const insertQuery = (res, data, table, dataUser) => {
  if (!empty(data) && !empty(table)){
    let dataLog = {
      'person_id': dataUser['id'],
      'type': 'insert',
      'table_afected': table,
      'log_id': dataUser['log_id'],
      'data_new': data
    };
    let nameValues = '';
    let values = '';

    Object.keys(data).map(function(key, index) {
      nameValues += key + ', ';
      values += '${' + key + '}, ';
    });
    
    nameValues = nameValues.substr(0, nameValues.length - 2);
    values = values.substr(0, values.length - 2);
    
    let query = `INSERT INTO ${table} (${nameValues}) VALUES (${values}) RETURNING id`;
    //data['table'] = table;
    
    connection.tx(t => {
          return t.batch([
              t.one(query, data),
          ]);
      })
      .then(responseData => {
        if (!empty(responseData[0])) {
          console.log(dataLog)
          res.status(200).json(responseData[0]);
        } else {
          res.status(400).json({ 'success': false, 'error': 'Necessary data' });
        }
      })
      .catch(error => {
        console.log('ERROR:', error);
        res.status(500).json({ 'success': false, 'error': error });
      });
  }
};

const tokenValidation = (postValidation) => {
  return (req, res, next) => {
    if (!empty(req.headers.authorization)) {
      let dataHeader = req.headers.authorization;
      dataHeader = dataHeader.split(" ", 2); 
      let token = dataHeader[1];
      let query = `SELECT id, name, surname, username FROM person WHERE token = $(token) AND state = true`
      connection.any(query, { token })
        .then(data => {
          if(!empty(data)) {
            postValidation(req, res, next, data[0]);
          } else {
            res.status(401).json();
          }
        })
        .catch(err => next(err))
    } else {
      res.status(401)
    }
  }
}


export {
  connection,
  tokenValidation,
  insertQuery,
  selectQuery,
};