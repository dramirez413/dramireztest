import Express from "express";
import authController from "../controllers/auth";
import { tokenValidation } from "../models/connection";

const router = new Express.Router();

router.post(`/login`, authController.login);
router.post(`/token`, tokenValidation(authController.token));
router.put(`/logout`, tokenValidation(authController.logout));
router.post(`/pass`, authController.password);
router.get(`/test`, authController.test);

export default router;