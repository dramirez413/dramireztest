import Express from "express";
import { tokenValidation } from '../models/connection';
import newsController from "../controllers/news";

const router = new Express.Router();

router.get(`/list`, tokenValidation(newsController.getList));
router.post(``, tokenValidation(newsController.insert));
router.put(``, tokenValidation(newsController.update));
router.put(`/delete`, tokenValidation(newsController.eliminate));
router.patch(``, tokenValidation(newsController.state));


export default router;