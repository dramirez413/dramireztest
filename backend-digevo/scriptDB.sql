CREATE TABLE person (
    id SERIAL,
    name VARCHAR(45),
    surname VARCHAR(45),
    username VARCHAR(45),
    password VARCHAR(256),
    token VARCHAR(512),
    state BOOLEAN DEFAULT TRUE,
    CONSTRAINT pk_person PRIMARY KEY (id)
);

CREATE TABLE permission (
    id SERIAL,
    name VARCHAR(45),
    CONSTRAINT pk_permission PRIMARY KEY (id)
);

CREATE TABLE person_permission (
    id SERIAL,
    personId INT,
    permissionId INT,
    CONSTRAINT pk_person_permission PRIMARY KEY (id),
    CONSTRAINT fk_person_permission FOREIGN KEY (personId) REFERENCES person(id),
    CONSTRAINT fk_permission_person FOREIGN KEY (permissionId) REFERENCES permission(id)
);

CREATE TABLE news (
    id SERIAL,
    personId INT,
    title VARCHAR(256),
    description TEXT,
    state BOOLEAN DEFAULT TRUE,
    CONSTRAINT pk_news PRIMARY KEY (id),
    CONSTRAINT fk_news_person FOREIGN KEY (personId) REFERENCES person(id)
);

INSERT INTO permission (name) VALUES ('read');
INSERT INTO permission (name) VALUES ('edit');

INSERT INTO person (name, surname, username, password, token, state) VALUES ('Diego', 'Ramirez', 'dramirez', '$2a$10$1/HYT.llwISFcKPyOXXKX.sf8DwhR0wm0GSQHyKoWr6P3mbl9V8pW', '', true);

INSERT INTO person_permission (personId, permissionId) VALUES (1, 1);
INSERT INTO person_permission (personId, permissionId) VALUES (1, 2);

INSERT INTO news (personId, title, description) VALUES (1, 'test', 'dtest');
INSERT INTO news (personId, title, description) VALUES (1, 'test', 'dtest');
INSERT INTO news (personId, title, description) VALUES (1, 'test', 'dtest');
INSERT INTO news (personId, title, description) VALUES (1, 'test', 'dtest');